# coding=utf-8
from floor24.models import Country, Announcement, Area, Building, BuildingType, City, CityRegion, District, Region, StaticPage, Street, Photo
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotAllowed, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.utils.encoding import smart_str
import django.utils.simplejson as json
from datetime import datetime as current_date
import md5, imghdr, os
from PIL import Image
import floor24.settings as settings

# список стран в формате JSON
def get_countries(request):
    try:
        countries = Country.objects.all()
        jsonDict = []
    except:
        return HttpResponse('{}', mimetype="application/json")

    for country in countries:
        jsonDict.append({'name': country.name, 'value': country.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json", )

# список округов в формате JSON
def get_districts(request):
    try:
        country_id = int(request.GET.get('country_id'))
        districts = District.objects.filter(country_id=country_id)
        jsonDict = []

    except TypeError:
        raise Http404
    except:
        return HttpResponse('{}', mimetype="application/json")

    for district in districts:
        jsonDict.append({'name': district.name, 'value': district.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json", )

# список областей в формате JSON
def get_areas(request):
    try:
        district_id = int(request.GET.get('district_id'))
        areas = Area.objects.filter(district_id=district_id)
        jsonDict = []
    except TypeError:
        raise Http404
    except:
        return HttpResponse('{}', mimetype="application/json")

    for area in areas:
        jsonDict.append({'name': area.name, 'value': area.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json", )

# список районов города и улиц в формате JSON
def get_cityregions_and_streets(request):
    try:
        city_id = int(request.GET.get('city_id'))
        streets = Street.objects.filter(city_id=city_id)
        city_regions = CityRegion.objects.filter(city_id=city_id)
        jsonDict = [
            [],
            [],
        ]
    except TypeError:
        raise Http404
    except:
        return HttpResponse('{}', mimetype="application/json")

    for street in streets:
        jsonDict[0].append({'name': street.name, 'value': street.id})
    for city_region in city_regions:
        jsonDict[1].append({'name': city_region.name, 'value': city_region.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json", )

# список районов и городов в формате JSON
def get_regions_and_cities(request):
    try:
        area_id = int(request.GET.get('area_id'))
        regions = Region.objects.filter(area_id=area_id)
        cities = City.objects.filter(area_id=area_id)
        jsonDict = [
            [],
            [],
        ]
    except TypeError:
        raise Http404
    except:
        return HttpResponse('{}', mimetype="application/json")

    for region in regions:
        jsonDict[0].append({'name': region.name, 'value': region.id})
    for city in cities:
        jsonDict[1].append({'name': city.name, 'value': city.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json")

# список областей в формате JSON
def get_buildings(request):
    try:
        street_id = int(request.GET.get('street_id'))
        buildings = Building.objects.filter(street_id=street_id)
        jsonDict = []
    except TypeError:
        raise Http404
    except:
        return HttpResponse('{}', mimetype="application/json")

    for building in buildings:
        jsonDict.append({'name': building.number, 'value': building.id})

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json", )

def get_additional_fields(request):
    try:
        building_type_id = int(request.GET.get('building_type'))
        building_type = BuildingType.objects.filter(id=building_type_id)[0]
        fields = json.loads(smart_str(building_type.fields))
        add_fields = {}

        if len(building_type.add_fields):
            add_fields = json.loads(building_type.add_fields)

        jsonDict = {
            'response': True,
            'fields': fields,
            'add_fields': add_fields,
        }
    except:
        jsonDict = {'response': False}

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json")

# добавление в БД нового элемента местоположения
def new_geoitem(request):
    a_list = [
        'new_country',
        'new_district',
        'new_area',
        'new_region',
        'new_city',
        'new_cityregion',
        'new_street',
        'new_building',
    ]

    if smart_str(request.GET.get('action')) not in a_list:
        raise Http404

    try:
        action = smart_str(request.GET.get('action'))
        name = smart_str(request.GET.get('value'))

        if action == 'new_country':
            o = Country(name=name)
        elif action == 'new_district':
            country_id = int(request.GET.get('country_id'))
            o = District(name=name, country_id=country_id)
        elif action == 'new_area':
            district_id = int(request.GET.get('district_id'))
            o = Area(name=name, district_id=district_id)
        elif action == 'new_region':
            area_id = int(request.GET.get('area_id'))
            o = Region(name=name, area_id=area_id)
        elif action == 'new_city':
            area_id = int(request.GET.get('area_id'))
            o = City(name=name, area_id=area_id)
        elif action == 'new_cityregion':
            city_id = int(request.GET.get('city_id'))
            o = CityRegion(name=name, city_id=city_id)
        elif action == 'new_street':
            city_id = int(request.GET.get('city_id'))
            o = Street(name=name, city_id=city_id)
        elif action == 'new_building':
            street_id = int(request.GET.get('street_id'))
            o = Building(number=name, street_id=street_id)
        else:
            raise

        o.save()
        jsonDict = {
            'response': True,
            'value': name
        }
    except TypeError:
        raise Http404
    except:
        jsonDict = {'response': False}

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json")

def upload_photo(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')

    try:
        f = request.FILES['upload_photo_field']
        date = current_date.now()
        filename = md5.new('%04d-%02d-%02d %02d:%02d:%02d:%02d' % (date.year, date.month, date.day,
                                                                   date.hour, date.minute, date.second,
                                                                   date.microsecond)).hexdigest()

        IMAGE_MEDIA_PATH = 'media/images/' # TODO сделать аналог переменной settings.AVATAR_UPLOAD_DIR
        with open(settings.MEDIA_ROOT + 'images/' + str(filename) + '.jpg', 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

            if imghdr.what(settings.MEDIA_ROOT + 'images/' + filename + '.jpg') != 'jpeg':
               os.remove(destination)
               raise

        min_size = {
            'big': {
                'height': 485,
                'width': 593
            },
            'medium': { # с кропом
                'height': 140,
                'width': 206
            },
            'small': {
                'height': 80,
                'width': 207
            },
        }

        image = Image.open(settings.MEDIA_ROOT + 'images/' + filename + '.jpg')

        if image.mode not in ('L', 'RGB'):
            image = image.convert('RGB')
        # определеняем соотношение сторон
        wk = image.size[0]/float(min_size['big']['width'])
        hk = image.size[1]/float(min_size['big']['height'])
        wk_medium = image.size[0]/float(min_size['medium']['width'])
        hk_medium = image.size[1]/float(min_size['medium']['height'])
        wk_small = image.size[0]/float(min_size['small']['width'])
        hk_small = image.size[1]/float(min_size['small']['height'])

        if wk > 1 or hk > 1:
            if wk > hk: # обрезаем вертикально
                new_size = (min_size['big']['width'], int(image.size[1]/wk))
            else: # обрезаем горизонтально
                new_size = (int(image.size[0]/hk), min_size['big']['height'])
        else:
            new_size = image.size

        if wk_small > 1 or hk_small > 1:
            if wk_small > hk_small: # обрезаем вертикально
                new_size_small = (min_size['small']['width'], int(image.size[1]/wk_small))
            else: # обрезаем горизонтально
                new_size_small = (int(image.size[0]/hk_small), min_size['small']['height'])
        else:
            new_size_small = image.size

        if wk_medium > 1 or hk_medium > 1:
            if wk_medium > hk_medium: # обрезаем вертикально
                new_size_medium = (int(image.size[0]/hk_medium), min_size['medium']['height'])
            else: # обрезаем горизонтально
                new_size_medium = (min_size['medium']['width'], int(image.size[1]/wk_medium))
        else:
            new_size_medium = image.size

        crop_box = (
            (new_size_medium[0] - min_size['medium']['width']) / 2,
            (new_size_medium[1] - min_size['medium']['height']) / 2,
            min_size['medium']['width'] + (new_size_medium[0] - min_size['medium']['width']) / 2,
            min_size['medium']['height'] + (new_size_medium[1] - min_size['medium']['height']) / 2,
        )
        image.resize(new_size, Image.ANTIALIAS).save(settings.MEDIA_ROOT + 'images/' + filename + '_big.jpg')
        image.resize(new_size_small, Image.ANTIALIAS).save(settings.MEDIA_ROOT + 'images/' + filename + '_small.jpg')
        image.resize(new_size_medium, Image.ANTIALIAS).crop(crop_box).save(settings.MEDIA_ROOT + 'images/' + filename + '_medium.jpg')

        os.remove(settings.MEDIA_ROOT + 'images/' + filename + '.jpg')

        p = Photo(filename=filename)
        p.save()

        jsonDict = {
            'response': True,
            'filename': filename,
        }
    except Photo.DoesNotExist:
        jsonDict = {'response': False}

    return HttpResponse(json.dumps(jsonDict))

def add_page(request):
    try:
        title = smart_str(request.POST.get('title'))
        link = smart_str(request.POST.get('link'))
        meta = smart_str(request.POST.get('meta'))
        description = smart_str(request.POST.get('description'))
        keywords = smart_str(request.POST.get('keywords'))
        content = smart_str(request.POST.get('content'))
        p = StaticPage(title=title, link=link, meta=meta, description=description, keywords=keywords, content=content)
        p.save()
    except:
        pass
    jsonDict = {'response': True}

    return HttpResponse(json.dumps(jsonDict))

# редактирование анкеты
def save_announcement(request):
    try:
        photos = json.loads(smart_str(request.POST.get('photos')))
    except:
        photos = []

    try:
        date = current_date.now()
        now_date = '%04d-%02d-%02d' % (date.year, date.month, date.day)

        call_date = smart_str(request.POST.get('call_date'))
        announcement_id = int(request.POST.get('announcement_id'))
        fields = smart_str(request.POST.get('additional'))
        manager_fio = smart_str(request.POST.get('manager_fio'))
        manager_telephone = smart_str(request.POST.get('manager_telephone'))
        manager_email = smart_str(request.POST.get('manager_email'))
        subject = smart_str(request.POST.get('subject'))
        description = smart_str(request.POST.get('description'))
        status = int(request.POST.get('status'))
        type_a = int(request.POST.get('type_a'))
        price = int(request.POST.get('price'))
        price_currency = int(request.POST.get('price_currency'))
        client_telephone = smart_str(request.POST.get('client_telephone'))
        client_fio = smart_str(request.POST.get('client_fio'))

        contract = smart_str(request.POST.get('contract'))
        vip = bool(request.POST.get('vip'))
        vip_sort = int(request.POST.get('vip_sort'))
        commission = int(request.POST.get('commission'))
        commission_currency = int(request.POST.get('commission_currency'))

        building_type_id = int(request.POST.get('building_type'))
        city_region_id = int(request.POST.get('cityregion'))
        region_id = int(request.POST.get('region'))
        building_id = int(request.POST.get('building'))

        building_type = BuildingType.objects.get(id=building_type_id)
        building = Building.objects.get(id=building_id)
        city_region = CityRegion.objects.get(id=city_region_id)
        region = Region.objects.get(id=region_id)

        main_photo = ''
        if len(photos):
            main_photo = photos[0]['filename']

        if call_date == '':
            call_date = None
        else:
            call_date = current_date.strptime(call_date, '%d.%m.%Y').strftime('%Y-%m-%d')

        a = Announcement.objects.filter(id=announcement_id)

        a.update(manager_email=manager_email, manager_fio=manager_fio, manager_telephone=manager_telephone,
                subject=subject, description=description, status=status, building_type=building_type, type_a=type_a,
                city_region=city_region, region=region, building=building, price=price,
                price_currency=price_currency, client_telephone=client_telephone, client_fio=client_fio,
                contract=contract, vip=vip, vip_sort=vip_sort, commission=commission, call_date=call_date,
                commission_currency=commission_currency, creation_date=now_date, fields=fields, main_photo=main_photo)

        #TODO: сделать чтобы удалялись фотки при изменинии

        if len(photos):
            for photo in photos:
                p = Photo.objects.get(filename=photo['filename'])
                p.announcement = a[0]
                p.save()

        jsonDict = {'response': True}
    except Photo.DoesNotExist:
        jsonDict = {'response': False}

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json")

# добавление новой анкеты
def add_announcement(request):
    try:
        photos = json.loads(smart_str(request.POST.get('photos')))
    except:
        photos = []

    try:
        city_region = CityRegion.objects.get(id=int(request.POST.get('cityregion')))
    except:
        city_region = None

    try:
        region = Region.objects.get(id=int(request.POST.get('region')))
    except:
        region = None

    try:
        date = current_date.now()
        now_date = '%04d-%02d-%02d' % (date.year, date.month, date.day)

        fields = smart_str(request.POST.get('additional'))
        manager_fio = smart_str(request.POST.get('manager_fio'))
        manager_telephone = smart_str(request.POST.get('manager_telephone'))
        manager_email = smart_str(request.POST.get('manager_email'))
        subject = smart_str(request.POST.get('subject'))
        description = smart_str(request.POST.get('description'))
        status = int(request.POST.get('status'))
        type_a = int(request.POST.get('type_a'))
        price = int(request.POST.get('price'))
        price_currency = int(request.POST.get('price_currency'))
        client_telephone = smart_str(request.POST.get('client_telephone'))
        client_fio = smart_str(request.POST.get('client_fio'))
        call_date = smart_str(request.POST.get('call_date'))
        contract = smart_str(request.POST.get('contract'))
        vip = bool(request.POST.get('vip'))
        vip_sort = int(request.POST.get('vip_sort'))
        commission = int(request.POST.get('commission'))
        commission_currency = int(request.POST.get('commission_currency'))

        building_type_id = int(request.POST.get('building_type'))
        building_id = int(request.POST.get('building'))

        building_type = BuildingType.objects.get(id=building_type_id)
        building = Building.objects.get(id=building_id)

        main_photo = ''
        if len(photos):
            main_photo = photos[0]['filename']

        if call_date == '':
            call_date = None
        else:
            call_date = current_date.strptime(call_date, '%d.%m.%Y').strftime('%Y-%m-%d')

        a = Announcement(manager_email=manager_email, manager_fio=manager_fio, manager_telephone=manager_telephone,
                subject=subject, description=description, status=status, building_type=building_type, type_a=type_a,
                city_region=city_region, region=region, building=building, price=price,
                price_currency=price_currency, client_telephone=client_telephone, client_fio=client_fio,
                call_date=call_date, contract=contract, vip=vip, vip_sort=vip_sort, commission=commission,
                commission_currency=commission_currency, creation_date=now_date, fields=fields, main_photo=main_photo)
        a.save()

        if len(photos):
            for photo in photos:
                p = Photo.objects.get(filename=photo['filename'])
                p.announcement = a
                p.save()

        jsonDict = {'response': True}
    except Photo.DoesNotExist:
        jsonDict = {'response': False}

    return HttpResponse(json.dumps(jsonDict), mimetype="application/json")

def get_ajax(request, action): # ответы на ajax-запросы
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')
    #if not request.is_ajax():
    #    return HttpResponseNotAllowed('Ajax')

    ajax_func = {
        'getcountries': get_countries,
        'getdistricts': get_districts,
        'getareas': get_areas,
        'getregionandcity': get_regions_and_cities,
        'getcregionsandsreets': get_cityregions_and_streets,
        'getbuildings': get_buildings,
        'newitem': new_geoitem,
        'getadditionalfields': get_additional_fields,
        'add_announcement': add_announcement,
        'save_announcement': save_announcement,
        'add_page': add_page,
        #'upload': upload_photo,
    }

    if action in ajax_func:
        return ajax_func[action](request)
    else:
        raise Http404