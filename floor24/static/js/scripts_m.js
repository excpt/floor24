function str_replace(search, replace, subject) {
    return subject.split(search).join(replace);
}

function get_select_ajax(data, action, select_name) {
    /*
    data - JSON object to send as GET parameter
    action - URL
    select_name - name of Select tag, where result will be written
    */
    window.str = '<option value="">---</option>';
    $.ajax({
        url: "/manage/ajax/" + action + '/',
        type: 'GET',
        data: data,
        dataType: 'json',
        success: function(data) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    window.str += '<option value="' + data[i].value + '">' + data[i].name + '</option>';
                }
            }
        }
    }).always(function(msg) {
        o = $('[name="' + select_name + '"]');
        o.html(window.str);
        o.val(0);
        o.trigger('change');
    });
}

function get_multi_select_ajax(data, action, select_name0, select_name1) {
    window.str_m = [];
    window.str_m[0] = '<option value="">---</option>';
    window.str_m[1] = window.str_m[0];
    $.ajax({
        url: "/manage/ajax/" + action + '/',
        type: 'GET',
        data: data,
        dataType: 'json',
        success: function(data) {
            //alert(data);
            if (typeof data[0] == 'undefined')
                return false;

            if ((data[0].length > 0) || (data[1].length > 0)) {
                for (var i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i].length; j++) {
                        window.str_m[i] += '<option value="' + data[i][j].value + '">' + data[i][j].name + '</option>';
                    }
                }
            }
        }
    }).always(function(msg) {
        o0 = $('[name="' + select_name0 + '"]');
        o1 = $('[name="' + select_name1 + '"]');
        o0.html(window.str_m[0]);
        o1.html(window.str_m[1]);
        o0.val(0);
        o1.val(0);
        o0.trigger('change');
        o1.trigger('change');
    });
}

function ajax_new_geoitem(action, o) {
    //alert($('select[name="district"]').val());
    var data = {
        value: $(o).prev().val(),
        action: action,
        country_id: $('select[name="country"]').val() ? $('select[name="country"]').val() : 0,
        district_id: $('select[name="district"]').val() ? $('select[name="district"]').val() : 0,
        area_id: $('select[name="area"]').val() ? $('select[name="area"]').val() : 0,
        city_id: $('select[name="city"]').val() ? $('select[name="city"]').val() : 0,
        street_id: $('select[name="street"]').val() ? $('select[name="street"]').val() : 0
    };
    $.ajax({
        url: "/manage/ajax/newitem/",
        type: 'GET',
        data: data,
        dataType: 'json',
        success: function(data) {
            var str = '';
            if (data.response == true) {
                var select_name;
                select_name = {
                    new_country: null,
                    new_district: 'country',
                    new_area: 'district',
                    new_region: 'area',
                    new_city: 'area',
                    new_cityregion: 'city',
                    new_street: 'city',
                    new_building: 'street'
                };
                if (select_name[action])
                    $('select[name="' + select_name[action] + '"]').trigger('change');
                else
                    get_select_ajax({}, 'getcountries', 'country');
                str = '<span class=info_ok>Запись "' + data.value + '" успешно добавлена!<span>';
                $(o).prev().val('');
            } else {
                str = '<span class=info_error>Произошла ошибка при добавлении!<span>';
            }
            $('#form_information').html(str);
        },
        error: function() {
            $('#form_information').html('<span class=info_error>Неизвестная ошибка!<span>');
        }
    });
}

$(document).ready(function() {
    $('.form_new_item').click(function() {
        if($(this).prev().val().length > 0) {
            var action = $(this).attr('action');
            ajax_new_geoitem(action, this);
        } else {
            $('#form_information').html('<span class=info_error>Заполните поле!<span>');
        }
    });

    $('[name="country"]').change(function() {
        var data = {country_id: $(this).val()};
        var list = [
            "area",
            "region",
            "city",
            "cityregion",
            "street",
            "building"
        ];

        get_select_ajax(data, 'getdistricts', 'district');
        for (var i = 0; i < list.length; i++) {
            $('[name="' + list[i] + '"]').html('<option value="">---</option>');
        }
    });

    $('[name="district"]').change(function() {
        var data = {district_id: $(this).val() ? $(this).val() : 0};
        if (get_select_ajax(data, 'getareas', 'area')) {
            $('[name="region"]').html('<option value="">---</option>');
            $('[name="city"]').html('<option value="">---</option>');
            $('[name="cityregion"]').html('<option value="">---</option>');
            $('[name="street"]').html('<option value="">---</option>');
            $('[name="building"]').html('<option value="">---</option>');
        }
    });

    $('[name="area"]').change(function() {
        var data = {area_id: $(this).val() ? $(this).val() : 0};
        if (get_multi_select_ajax(data, 'getregionandcity', 'region', 'city')) {
            $('[name="city"]').html('<option value="">---</option>');
            $('[name="cityregion"]').html('<option value="">---</option>');
            $('[name="street"]').html('<option value="">---</option>');
            $('[name="building"]').html('<option value="">---</option>');
        }
    });

    $('[name="city"]').change(function() {
        var data = {city_id: $(this).val() ? $(this).val() : 0};
        if (get_multi_select_ajax(data, 'getcregionsandsreets', 'street', 'cityregion')) {
            $('[name="street"]').html('<option value="">---</option>');
            $('[name="building"]').html('<option value="">---</option>');
        }
    });

    $('[name="street"]').change(function() {
        var data = {street_id: $(this).val() ? $(this).val() : 0};
        if (get_select_ajax(data, 'getbuildings', 'building')) {
            $('[name="building"]').html('<option value="">---</option>');
        }
    });

    $('select[name="building_type"]').change(function() {
        get_additional_fields($(this).val());
    });

    $('iframe[name="upload_target"]').load(function() {
        upload_photo_done();
    });
});
function upload_photo_done() {
    //alert();
    var t = $('iframe[name="upload_target"]').contents().find('body').html();
    //alert(t);
    if (t && t.length) {
        t = $.parseJSON(t);
        if(t.response) {
            $('#form_photos').append(
                '<input type="hidden" name="photo_filename" value="' + t.filename + '" />'
            );
            $('#form_photos_list').append(
                '<tr><td><img src="/media/images/' + t.filename + '_small.jpg"/></td><td><input type="button" onclick="delete_photo(' + t.filename + ')" value="Удалить фото"></td></tr>'
            );
            $('#form_information').html('<span class=info_ok>Фотография добавлена!<span>');
            $('input[name="upload_photo_field"]').val('');
        } else
            $('#form_information').html('<span class=info_error>Произошла ошибка при добавлении фотографии!<span>');
    }
    return false;
}

function get_additional_fields(building_type) {
    $.ajax({
        url: "/manage/ajax/getadditionalfields/",
        type: 'GET',
        data: {building_type: building_type},
        dataType: 'json',
        success: function(data) {
            if (data.response == true) {
                var fields_div = $('#form_additional_fields');
                fields_div.html('');
                for (var i = 0; i < data.fields.length; i++) {
                    var group = data.fields[i];
                    fields_div.append('<div id="fields_group_' + group.group_name + '"><h4>' +
                        group.group_caption + '</h4></div>')
                    var group_div = $('#fields_group_' + group.group_name)
                    for (var j = 0; j < group.fields.length; j++) {
                        var value = group.fields[j];
                        var field = '';
                        switch (value.type) {
                            case 'text':
                                field = value.caption + ':<input type="text" name="field_' +
                                    group.group_name + '_' + value.name + '"/><br/>';
                                break;
                            case 'int':
                                field = value.caption + ':<input type="text" name="field_' +
                                    group.group_name + '_' + value.name + '"/><br/>';
                                break;
                            case 'bool':
                                field = value.caption + ':<input type="checkbox" name="field_' +
                                    group.group_name + '_' + value.name + '"/><br/>';
                                break;
                            case 'enum':
                                if (value.values.length == 0)
                                    continue;

                                field = value.caption + ':<select name="field_' + group.group_name + '_' + value.name + '">';
                                field += '<option value="-1">---</option>';
                                for (var k = 0; k < value.values.length; k++) {
                                    var option = value.values[k];
                                    field += '<option value="' + option.value + '">' + option.caption + '</option>';
                                }
                                field += '</select><br/>';
                                break;
                            case 'set':
                                if (value.values.length == 0)
                                    continue;

                                field = value.caption + ':<br/><input type="hidden" name="field_' + group.group_name + '_' +
                                    value.name + '" />';

                                for (var k = 0; k < value.values.length; k++) {
                                    var check = value.values[k];
                                    field += check.caption + '<input type="checkbox" name="option_' + group.group_name + '_' +
                                        value.name + '_' + check.name + '"/><br/>';
                                }

                                break;
                        }
                        if (field.length > 0)
                            group_div.append(field);
                    }
                }
            }
        }
    });
}

// преобразование формы ввода в JSON
function process_additional_fields() {
    var fields_div = $('#form_additional_fields');
    var json = [];
    var i = 0;
    $('[id^="fields_group_"]').each(function() {
        //alert($(this).attr('id'))
        var id = $(this).attr('id');
        group_name = str_replace('fields_group_', '', id);
        json[i] = {};
        json[i].group_name = group_name;
        json[i].fields = [];
        var k = 0;

        $(this).find('[name^=field_' + group_name +']').each(function() {
            if ($(this).is('input')) {
                switch ($(this).attr('type')) {
                    case 'hidden':
                        var field_name = str_replace('field_' + group_name + '_', '', $(this).attr('name'));
                        var set_group = {name: field_name, type: 'set', values: []};
                        var iter = 0;
                        $('input[name^="option_' + group_name + '_' + field_name + '"]').each(function() {
                            var field_value = $(this).is(':checked');
                            set_group.values[iter] = {
                                name: str_replace('option_' + group_name + '_' + field_name + '_', '', $(this).attr('name')),
                                value: field_value
                            };
                            iter++;
                        });
                        json[i].fields[k] = set_group;
                        k++;

                        break;
                    case 'checkbox':
                        var field_name = str_replace('field_' + group_name + '_', '', $(this).attr('name'));
                        var field_value = $(this).is(':checked');
                        json[i].fields[k] = {name: field_name, value: field_value};
                        k++;
                        break;
                    case 'text':
                        var field_name = str_replace('field_' + group_name + '_', '', $(this).attr('name'));
                        var field_value = $(this).val();
                        json[i].fields[k] = {
                            name: field_name,
                            value: field_value
                        };
                        k++;
                        break;
                }
            } else if($(this).is('select')) {
                var field_name = str_replace('field_' + group_name + '_', '', $(this).attr('name'));
                var field_value = $(this).val();
                json[i].fields[k] = {name: field_name, value: field_value};
                k++;
            }
        });
        i++;
        //alert(group_name);
    });

    return json;
}

function process_photo_fields() {
    var images = [];
    var i = 0;
    //alert($('input[name="photo_filename"]').eq(0).val());
    $('input[name="photo_filename"]').each(function() {
        //alert();
        images[i] = {};
        images[i].filename = $(this).val();
        i++;
    });

    return images;
}

function add_new_page() {
    var data = {};
    data.title = $('input[name="title"]').val();
    data.link = $('input[name="link"]').val();
    data.meta = $('textarea[name="meta"]').val();
    data.description = $('textarea[name="description"]').val();
    data.keywords = $('textarea[name="keywords"]').val();
    data.content = $('textarea[name="content"]').val();
    data.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val();

    if (data.title == '' || data.link == '') {
        $('#form_information').html('<span class=info_error>Заполните все поля отмеченные *</span>');
        return false;
    }

    $.ajax({
        url: "/manage/ajax/add_page/",
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(data) {
            if(data.response == true) {
                $('#form_information').html('<span class=info_ok>Анкета успешно добавлена!<span>');
            } else
                $('#form_information').html('<span class=info_error>Произошла ошибка при добавлении анкеты!<span>');
        },
        error: function() {
            $('#form_information').html('<span class=info_error>Произошла ошибка при добавлении анкеты!<span>');
        }
    });

    return true;
}

function add_new_announcement() {
    var data = {};

    data.additional = process_additional_fields();
    if (data.additional.length > 0) {
        data.additional = JSON.stringify(data.additional);
    }

    data.photos = process_photo_fields();
    if (data.photos.length > 0) {
        data.photos = JSON.stringify(data.photos);
    }

    data.manager_fio = $('input[name="manager_fio"]').val();
    data.manager_telephone = $('input[name="manager_telephone"]').val();
    data.manager_email = $('input[name="manager_email"]').val();
    data.subject = $('input[name="subject"]').val();
    data.description = $('textarea[name="description"]').val();
    data.status = $('select[name="status"]').val();
    data.building_type = $('select[name="building_type"]').val();
    data.type_a = $('select[name="type_a"]').val();
    data.cityregion = $('select[name="cityregion"]').val();
    data.region = $('select[name="region"]').val();
    data.building = $('select[name="building"]').val();
    data.price = $('input[name="price"]').val();
    data.price_currency = $('select[name="price_currency"]').val();
    data.client_telephone = $('input[name="client_telephone"]').val();
    data.client_fio = $('input[name="client_fio"]').val();
    data.call_date = $('input[name="call_date"]').val();
    data.contract = $('input[name="contract"]').val();
    data.vip = $('input[name="vip"]').is(':checked');
    data.vip_sort = $('input[name="vip_sort"]').val() ? $('input[name="vip_sort"]').val() : 0;
    data.commission = $('input[name="commission"]').val();
    data.commission_currency = $('select[name="commission_currency"]').val();
    data.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val();

    if (data.subject == '' || data.description == '' || data.building_type == '' || data.building == '' || data.manager_fio == '' ||
        data.manager_telephone == '') {
        $('#form_information').html('<span class=info_error>Заполните все поля отмеченные *<span>');
        return false;
    }


    $.ajax({
        url: "/manage/ajax/add_announcement/",
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(data) {
            //alert(data.response);
            if(data.response == true) {
                $('#form_information').html('<span class=info_ok>Анкета успешно добавлена!<span>')
            } else
                $('#form_information').html('<span class=info_error>Произошла ошибка при добавлении анкеты!<span>');
        },
        error: function() {
            $('#form_information').html('<span class=info_error>Произошла ошибка при добавлении анкеты!<span>');
        }

    });

    return true;
}


function save_announcement() {
    var data = {};

    data.additional = process_additional_fields();
    if (data.additional.length > 0) {
        data.additional = JSON.stringify(data.additional);
    }

    data.photos = process_photo_fields();
    if (data.photos.length > 0) {
        data.photos = JSON.stringify(data.photos);
    }

    data.announcement_id = $('input[name="announcement_id"]').val();
    data.manager_fio = $('input[name="manager_fio"]').val();
    data.manager_telephone = $('input[name="manager_telephone"]').val();
    data.manager_email = $('input[name="manager_email"]').val();
    data.subject = $('input[name="subject"]').val();
    data.description = $('textarea[name="description"]').val();
    data.status = $('select[name="status"]').val();
    data.building_type = $('select[name="building_type"]').val();
    data.type_a = $('select[name="type_a"]').val();
    data.cityregion = $('select[name="cityregion"]').val();
    data.region = $('select[name="region"]').val();
    data.building = $('select[name="building"]').val();
    data.price = $('input[name="price"]').val();
    data.price_currency = $('select[name="price_currency"]').val();
    data.client_telephone = $('input[name="client_telephone"]').val();
    data.client_fio = $('input[name="client_fio"]').val();
    data.call_date = $('input[name="call_date"]').val();
    data.contract = $('input[name="contract"]').val();
    data.vip = $('input[name="vip"]').is(':checked');
    data.vip_sort = $('input[name="vip_sort"]').val() ? $('input[name="vip_sort"]').val() : 0;
    data.commission = $('input[name="commission"]').val();
    data.commission_currency = $('select[name="commission_currency"]').val();
    data.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val();

    if (data.subject == '' || data.description == '' || data.building_type == '' || data.building == '' || data.manager_fio == '' ||
        data.manager_telephone == '') {
        $('#form_information').html('<span class=info_error>Заполните все поля отмеченные *<span>');
        return false;
    }

    $.ajax({
        url: "/manage/ajax/save_announcement/",
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(data) {
            if(data.response == true) {
                $('#form_information').html('<span class=info_ok>Изменения в анкета успешно сохранены!<span>')
            } else
                $('#form_information').html('<span class=info_error>Произошла ошибка при изменении в анкете!<span>');
        },
        error: function() {
            $('#form_information').html('<span class=info_error>Произошла ошибка при изменении в анкете!<span>');
        }
    });

    return true;
}


/*
пример нескольких групп:
[
    {
        group_name: '',
        group_caption: '',
        fields: [
            {type: 'int', name: '', caption: ''},
            {type: 'text', name: '', caption: ''},
            {type: 'bool', name: '', caption: ''},
        ]
    },
    {
        group_name: '',
        group_caption: '',
        fields: [
            {type: 'int', name: '', caption: ''},
            {type: 'text', name: '', caption: ''},
            {type: 'bool', name: '', caption: ''},
        ]
    },
]

Поля:

{type: 'int', name: '', caption: ''},

{type: 'text', name: '', caption: ''},

{type: 'bool', name: '', caption: ''},

{
    type: 'enum',
    name: '',
    caption: '',
    values: [
        {value: 0, caption: ''},
        {value: 1, caption: ''},
    ]
},

{
    type: 'set',
    name: '',
    caption: '',
    values: [
        {name: '', value: 0, caption: ''},
        {name: '', value: 1, caption: ''},
    ]
},
*/