# coding=utf-8
from django.db import models

class Country(models.Model): # страна
    name = models.CharField(max_length=50, verbose_name='Страна/Республика')

    class Meta:
        verbose_name_plural = u"Страны/Республики"
        verbose_name = u"Страна/Респулика"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class District(models.Model): # округ
    name = models.CharField(max_length=150, verbose_name='округ')
    country = models.ForeignKey(Country, verbose_name='Страна/Республика')

    class Meta:
        verbose_name_plural = u"Округа"
        verbose_name = u"Округ"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class Area(models.Model): # область
    name = models.CharField(max_length=150, verbose_name='область')
    district = models.ForeignKey(District, verbose_name='округ')

    class Meta:
        verbose_name_plural = u"Области"
        verbose_name = u"Область"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class Region(models.Model): # район области
    name = models.CharField(max_length=150, verbose_name='район области')
    area = models.ForeignKey(Area, verbose_name='область')

    class Meta:
        verbose_name_plural = u"Районы областей"
        verbose_name = u"Район области"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class City(models.Model): # Город/Населенный пункт
    name = models.CharField(max_length=150, verbose_name='город')
    region = models.ForeignKey(Region, verbose_name='Район области', blank=True, null=True)
    area = models.ForeignKey(Area, verbose_name='область')

    class Meta:
        verbose_name_plural = u"Города/Населеные пункты"
        verbose_name = u"Город/населенный пункт"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class CityRegion(models.Model): # Район города
    name = models.CharField(max_length=150, verbose_name='Район города')
    city = models.ForeignKey(City, verbose_name='город')

    class Meta:
        verbose_name_plural = u"Районы городов"
        verbose_name = u"Район города"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class Street(models.Model):
    name = models.CharField(max_length=300)
    city_region = models.ForeignKey(CityRegion, verbose_name='Район города', blank=True, null=True)
    city = models.ForeignKey(City, verbose_name='Город')


    class Meta:
        verbose_name_plural = u"Улицы"
        verbose_name = u"Улица"
        ordering = ['name']

    def __unicode__(self):
        return self.name

class Building(models.Model):
    number = models.CharField(max_length=50, verbose_name='Строение/Дом')
    street = models.ForeignKey(Street, verbose_name='улица')

    class Meta:
        verbose_name_plural = u"Строения/Дома"
        verbose_name = u"Строение/Дом"
        ordering = ['number']

    def __unicode__(self):
        return self.number

class StaticPage(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок страницы')
    meta = models.TextField(max_length=1000, verbose_name='Meta', blank=True, null=True)
    description = models.TextField(max_length=1000, verbose_name='Описание', blank=True, null=True)
    keywords = models.TextField(max_length=1000, verbose_name='Ключевые слова', blank=True, null=True)
    parent_id = models.IntegerField(verbose_name='Родительская страница', blank=True, null=True)
    link = models.CharField(max_length=100, verbose_name='Ссылка')
    template = models.CharField(max_length=100, verbose_name='Шаблон отображения', blank=True, null=True)
    content = models.TextField(max_length=100000, verbose_name='Содержимое страницы', blank=True, null=True)

    class Meta:
        verbose_name_plural = u"Статические страницы"
        verbose_name = u"Статическая страница"

    def __unicode__(self):
        return self.title

class BuildingType(models.Model):
    name = models.CharField(max_length=300, verbose_name='Название')
    eng_name = models.CharField(max_length=300, verbose_name='Название на английском')
    fields = models.TextField(max_length=5000, verbose_name='Поля в формате JSON', blank=True, null=True) # поля в формате JSON
    add_fields = models.TextField(max_length=5000, verbose_name='Поля в формате JSON', blank=True, null=True) # поля в формате JSON

    class Meta:
        verbose_name_plural = u"Типы недвижимости"
        verbose_name = u"Типы недвижимости"
        ordering = ['name']


    def __unicode__(self):
        return self.name

TYPE_A_CHOISES = (
    (0, 'Продажа'),
    (1, 'Аренда'),
)

CURRENCY_CHOISES = (
    (0, 'RUB'),
    (1, '$'),
    (2, '€'),
)

XML_OUT_CHOISES = (
    (0, 'afy.ru'),
    (1, 'cian.ru'),
    (2, 'redbrush'),
)

STATUS_CHOISES = (
    (0, 'Не активно'),
    (1, 'Активно'),
    (2, 'Продано/сдано'),
)

class Announcement(models.Model):
    # Информация о клиенте
    client_fio = models.TextField(max_length=300, verbose_name='ФИО', blank=True, null=True)
    client_telephone = models.CharField(max_length=20, verbose_name='Телефон', blank=True, null=True)
    call_date = models.DateField(verbose_name='Перезвонить', blank=True, null=True)
    contract = models.TextField(max_length=300, verbose_name='№ договора', blank=True, null=True)

    # Информация о менеджере
    subject = models.CharField(max_length=300, verbose_name='Тема')
    change_status_date = models.DateField(blank=True, null=True)
    manager_fio = models.TextField(max_length=300, verbose_name='ФИО')
    manager_telephone = models.CharField(max_length=20, verbose_name='Телефон')
    manager_mobile = models.CharField(max_length=20, verbose_name='Мобильный')
    manager_icq = models.CharField(max_length=20, verbose_name='ICQ', blank=True, null=True)
    manager_skype = models.CharField(max_length=30, verbose_name='Skype', blank=True, null=True)
    manager_email = models.EmailField(verbose_name='Email', blank=True, null=True)

    # Общая информация
    type_a = models.IntegerField(choices=TYPE_A_CHOISES, verbose_name='Тип анкеты')
    status = models.IntegerField(choices=STATUS_CHOISES, verbose_name='Активность')
    building_type = models.ForeignKey(BuildingType, verbose_name='Тип недвижимости')
    fields = models.TextField(max_length=10000, blank=True, null=True)
    description = models.TextField(max_length=1000, verbose_name='Описание')
    square = models.IntegerField(verbose_name='Площадь', blank=True, null=True)
    floor = models.IntegerField(verbose_name='Этаж', blank=True, null=True)
    room_count = models.IntegerField(verbose_name='Количество комнат', blank=True, null=True)
    author = models.IntegerField(verbose_name='Автор', blank=True, null=True)
    building = models.ForeignKey(Building, verbose_name='Строение/Дом')
    region = models.ForeignKey(Region, verbose_name='Район области', blank=True, null=True)
    city_region = models.ForeignKey(CityRegion, verbose_name='Район города', blank=True, null=True)
    price_currency = models.IntegerField(choices=CURRENCY_CHOISES, verbose_name='Валюта', blank=True, null=True)
    price = models.IntegerField(verbose_name='Стоимость', blank=True, null=True)
    price_m = models.IntegerField(verbose_name='Стоимость за м2', blank=True, null=True)
    main_photo = models.CharField(max_length=40, verbose_name='Главная фотография', blank=True, null=True)
    vip = models.BooleanField(verbose_name='VIP')
    commission = models.IntegerField(verbose_name='Комисия', blank=True, null=True)
    commission_currency = models.IntegerField(choices=CURRENCY_CHOISES, verbose_name='Валюта комисии', blank=True, null=True)
    vip_sort = models.IntegerField(default=0, verbose_name='VIP положение', blank=True, null=True)
    xml_out = models.IntegerField(verbose_name='XML экспорт', blank=True, null=True)
    note = models.TextField(max_length=1000, verbose_name='Заметка', blank=True, null=True)
    creation_date = models.DateField(verbose_name='Дата создания')

    views = models.IntegerField(verbose_name='Количество показов', default=0)

    class Meta:
        verbose_name_plural = u"Анкеты"
        verbose_name = u"Анкета"

    def __unicode__(self):
        return '%s %s' % (self.building.street.name, self.building.number)


class Photo(models.Model):
    announcement = models.ForeignKey(Announcement, verbose_name='Анкета', blank=True, null=True)
    filename = models.CharField(max_length=16, verbose_name='Имя файла')

    class Meta:
        verbose_name_plural = u"Фотографии"
        verbose_name = u"Фотография"

    def __unicode__(self):
        return self.filename

FIELDS_TYPE_CHOISES = (
    (0, 'text'), #text input
    (1, 'int'),  #text input
    (2, 'bool'), #checkbox
    (3, 'enum'), #select
    (4, 'set'),  #many checkbox
)


