# coding=utf-8
from models import Country, Announcement, Area, Building, BuildingType, City, CityRegion, District, Region, \
    StaticPage, Street, Photo
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotAllowed, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.forms.models import model_to_dict
from django.utils.encoding import smart_str
import django.utils.simplejson as json
from datetime import datetime as current_date

def manage_index(request):
    if not request.user.is_authenticated():
        return render_to_response('login_form.html', {}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/manage/announcements/')

def manage_pages_list(request):
    return HttpResponse('TEST')

def manage_announcements_list(request): # список анкет
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')

    information = smart_str(request.GET.get('msg', ''))

    list = Announcement.objects.all()
    return render_to_response('announcements_list.html', {'announcements': list, 'auth': True, 'information': information})

def manage_new_announcement(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')
    countries = Country.objects.all()
    building_types = BuildingType.objects.all()
    return render_to_response('new_announcement_form.html', {'countries': countries,
                                                             'building_types': building_types,
                                                             'auth': True,
    },
        context_instance=RequestContext(request))

def manage_edit_announcement(request, id):
    a = Announcement.objects.get(id=id)
    fields_groups = json.loads(BuildingType.objects.get(id=a.building_type.id).fields)
    fields_values = json.loads(a.fields)
    fields = []

    a_country = Country.objects.get(id=a.building.street.city.area.district.country_id)
    a_district = District.objects.get(id=a.building.street.city.area.district_id)
    a_area = Area.objects.get(id=a.building.street.city.area_id)
    #a_region = Region.objects.get(id=a.region)
    a_city = City.objects.get(id=a.building.street.city_id)
    #a_city_region = CityRegion.objects.get(id=a.city_region)
    a_street = Street.objects.get(id=a.building.street_id)

    countries = Country.objects.all()
    districts = District.objects.filter(country=a_country)
    areas = Area.objects.filter(district=a_district)
    cities = City.objects.filter(area=a_area)
    streets = Street.objects.filter(city=a_city)
    buildings = Building.objects.filter(street=a_street)

    building_types = BuildingType.objects.all()

    try:
        regions = Region.objects.filter(area=a_area)
    except:
        regions = None

    try:
        city_regions = CityRegion.objects.filter(city=a_city)
    except:
        city_regions = None

    for fields_values_item in fields_values:
        for group in fields_groups:
            if fields_values_item['group_name'] != group['group_name']:
                continue

            group_list = {
                'group_name': group['group_name'],
                'caption': group['group_caption'],
                'fields': [],
            }

            for field in fields_values_item['fields']:
                if 'type' in field and field['type'] == 'set':
                    set = {'name': field['name'], 'type': 'set', 'values': []}
                    for field_main in group['fields']:
                        if field['name'] == field_main['name']:
                            set['caption'] = field_main['caption']
                            for sub_set in field['values']:
                                for sub_set_main in field_main['values']:
                                    if sub_set['name'] == sub_set_main['name']:
                                        sub_set_main['value'] = sub_set['value']
                                        set['values'].append(sub_set_main)
                    group_list['fields'].append(set)
                elif 'value' in field:
                    for field_main in group['fields']:
                        if field['name'] == field_main['name']:
                            if field_main['type'] == 'enum':
                                field_main['value'] = int(field['value'])
                                group_list['fields'].append(field_main)
                            else:
                                field_main['value'] = field['value']
                                group_list['fields'].append(field_main)

            #if len(group_list['fields']):
            fields.append(group_list)
                #if field.type == 'int' or field.type == 'bool' or field.type == 'text':

    try:
        photos = Photo.objects.filter(announcement=a)
    except:
        photos = None

    return render_to_response('edit_announcement.html', {
        'auth': True,
        'announcement': a,
        'photos': photos,
        'fields': fields,
        'building_types': building_types,
        'countries': countries,
        'districts': districts,
        'areas': areas,
        'regions': regions,
        'cities': cities,
        'city_regions': city_regions,
        'streets': streets,
        'buildings': buildings,
    }, context_instance=RequestContext(request))

def manage_permanent_delete_announcement(request, id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')

    try:
        a = Announcement.objects.get(id=id)
        a.delete()
        information = 'Анкета удалена!'
    except:
        information = 'Ошибка удаления анкеты!'

    return HttpResponseRedirect('/manage/announcements/?msg=' + information)

def manage_delete_announcement(request, id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')

    a = Announcement.objects.get(id=id)
    return render_to_response('delete_announcement.html', {'announcement': a}, context_instance=RequestContext(request))

def manage_new_page(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/')

    return render_to_response('new_page_form.html', {'auth': True}, context_instance=RequestContext(request))

def public_page_view(request, page_link):
    #TODO: редирект после логина, редирект после добавления анкеты на список анкет
    p = StaticPage.objects.get(link=page_link)
    return render_to_response('page_view.html', {
        'content': p.content,
        'title': p.title,
        'keywords': p.keywords,
        'description': p.description,
    })

def public_index(request):
    return render_to_response('index_public.html', {'index_page': True, 'search_form': False}, context_instance=RequestContext(request))
    #TODO: вывести на главную курс валют, кол-во анкет в базе

# отображение списка анкет на публичной части
def public_announcements_list(request, type, building_type=None):
    try:
        page = int(request.GET.get('page'))
    except:
        page = 0

    if type == 'sell':
        type = 0
    else:
        type = 1

    try:
        if building_type is not None:
            b = BuildingType.objects.get(eng_name=building_type)
            a = Announcement.objects.filter(type_a=type, building_type=b, status=1)
        else:
            a = Announcement.objects.filter(type_a=type, status=1)
    except Announcement.DoesNotExist:
        raise Http404

    return render_to_response('announcements_public_list.html', {'announcements': a})

# отображение одной анкеты на публичной части
def public_announcement_view(request, id):
    try:
        a = Announcement.objects.get(id=id)
        photos = Photo.objects.filter(announcement=a)
        a.views = a.views + 1
        a.save()
        fields_values = json.loads(a.fields)
        fields_groups = json.loads(BuildingType.objects.get(id=a.building_type.id).fields)
        fields = []
    except Announcement.DoesNotExist:
        raise Http404
    except Photo.DoesNotExist:
        photos = None

    try:
        for fields_values_item in fields_values:
            for group in fields_groups:
                if fields_values_item['group_name'] != group['group_name']:
                    continue

                group_list = {
                    'group_name': group['group_name'],
                    'caption': group['group_caption'],
                    'fields': [],
                }

                #{"name":"trees","type":"set","values":[{"name":"leaf","value":true},{"name":"coniferous","value":true},{"name":"fruit","value":true}]}
                for field in fields_values_item['fields']:
                    if ('value' in field) and (field['value'] != '') and (field['value'] != '-1') and (field['value'] != False):
                        for field_main in group['fields']:
                            if field['name'] == field_main['name']:
                                if field_main['type'] == 'enum':
                                    #group_list['fields'].append(field_main)
                                    for enum_value in field_main['values']:
                                        if enum_value['value'] == int(field['value']):
                                            field_main['value'] = enum_value['caption']
                                            group_list['fields'].append(field_main)
                                            del field_main['values']
                                            break
                                else:
                                    field_main['value'] = field['value']
                                    group_list['fields'].append(field_main)
                    elif 'type' in field and field['type'] == 'set':
                        set = {'name': field['name'], 'type': 'set', 'values': []}
                        for field_main in group['fields']:
                            if field['name'] == field_main['name']:
                                set['caption'] = field_main['caption']
                                for sub_set in field['values']:
                                    for sub_set_main in field_main['values']:
                                        if sub_set['name'] == sub_set_main['name'] and sub_set['value']:
                                            sub_set_main['value'] = sub_set['value']
                                            set['values'].append(sub_set_main)
                        if len(set['values']):
                            group_list['fields'].append(set)

                if len(group_list['fields']):
                    fields.append(group_list)
                    #if field.type == 'int' or field.type == 'bool' or field.type == 'text':
    except:
        pass

    return render_to_response('announcement_public_view.html', {'announcement': a, 'photos': photos, 'fields': fields})

# аутентификация
def login(request):
    try:
        username = str(request.POST.get('username', ''))
        password = str(request.POST.get('password', ''))
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect('/manage/announcements/')
        else:
            return HttpResponse('you are not logined!')
    except:
        return HttpResponse('you are not logined!')

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


