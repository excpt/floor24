from django.contrib import admin
from floor24.models import Announcement, Area, Building, BuildingType, City, CityRegion, Country, District, Region,\
    StaticPage, Street, Photo

admin.site.register(Announcement)
admin.site.register(Area)
admin.site.register(Building)
admin.site.register(BuildingType)
admin.site.register(City)
admin.site.register(CityRegion)
admin.site.register(Country)
admin.site.register(District)
admin.site.register(Region)
admin.site.register(StaticPage)
admin.site.register(Street)
admin.site.register(Photo)
