from django.conf.urls import patterns, include, url
import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', 'floor24.views.public_index'),
    (r'^admin/', include(admin.site.urls)),

    (r'^manage/$', 'floor24.views.manage_index'),
    (r'^manage/announcements/edit/(?P<id>\d+)/$', 'floor24.views.manage_edit_announcement'),
    (r'^manage/announcements/delete/(?P<id>\d+)/$', 'floor24.views.manage_delete_announcement'),
    (r'^manage/announcements/p_delete/(?P<id>\d+)/$', 'floor24.views.manage_permanent_delete_announcement'),
    (r'^manage/announcements/$', 'floor24.views.manage_announcements_list'),

    (r'^manage/ajax/upload/$', 'floor24.ajax.views.upload_photo'),
    (r'^manage/ajax/(?P<action>\w+)/$', 'floor24.ajax.views.get_ajax'),
    (r'^manage/newannouncement/$', 'floor24.views.manage_new_announcement'),
    (r'^manage/newpage/$', 'floor24.views.manage_new_page'),

    (r'^pages/$', 'floor24.views.public_page_view'),
    (r'^pages/edit/(?P<id>\d+)/$', 'floor24.views.public_page_view'),
    (r'^pages/(?P<page_link>\w+)/$', 'floor24.views.public_page_view'),

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^logout/$', 'floor24.views.logout'),
    (r'^login/try/$', 'floor24.views.login'),

    (r'^rent/$', 'floor24.views.public_announcements_list', {'type': 'rent'}),
    (r'^rent/[a-z]+/(?P<id>\d+)/$', 'floor24.views.public_announcement_view',),
    (r'^rent/(?P<building_type>[a-z]+)/$', 'floor24.views.public_announcements_list', {'type': 'rent'}),

    (r'^sell/[a-z]+/(?P<id>\d+)/$', 'floor24.views.public_announcement_view'),
    (r'^sell/(?P<building_type>[a-z]+)/$', 'floor24.views.public_announcements_list', {'type': 'sell'}),
    (r'^sell/$', 'floor24.views.public_announcements_list', {'type': 'sell'}),


    # Examples:
    # url(r'^$', 'floor24.views.home', name='home'),
    # url(r'^floor24/', include('floor24.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)